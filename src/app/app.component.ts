import { Component } from '@angular/core';

@Component({selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
	title = 'huh';
	
	oscilator(dTime, dHertz, waveform) {
		switch(waveform) {
			case 'sine':
				return Math.sin(dTime * (dHertz * 2.0 * Math.PI)) ;
			default:
				return Math.sin(dTime * (dHertz * 2.0 * Math.PI)) ;
		}
	};

	play(){
		for(let i = 0; i <= 2145; i++) {
			console.log(this.oscilator(i, 144, 'sine')); // interval to be implemented
		}
	}
}
